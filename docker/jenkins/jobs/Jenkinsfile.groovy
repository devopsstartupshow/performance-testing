#!/usr/bin/env groovy

import org.jenkinsci.plugins.workflow.steps.FlowInterruptedException;
// import alex.jenkins.plugins.FileSystemListParameterDefinition;

// path to JMeter scenarios and test results in Jenkins container
String jmeterScenariosPath = '/var/lib/jmeter-scenarios'
String testResultsPath     = '/tmp/Results'     

// FileSystemListParameterDefinition fileSystemListParameterDefinition = new FileSystemListParameterDefinition('SCENARIO', "JMeter scenario to run", "master", jmeterScenariosPath, "FILE", "SINGLE_SELECT", ".+.jmx", "", true, false);

// properties([
//     buildDiscarder(logRotator(numToKeepStr: '50', artifactNumToKeepStr: '3')),
    // parameters([
    //     string(defaultValue: '10', name: 'USERS', description: '[JMeter property] Set total number of users'),
    //     string(defaultValue: '30', name: 'RAMPUP', description: '[JMeter property] Set ramp up duration(sec)'),
    //     string(defaultValue: '60', name: 'DURATION', description: '[JMeter property] Set the duration of user interaction with site'),
    //     string(name: 'HOST', description: '[JMeter property] Set endpoint DNS name'),
    //     string(name: 'IP', description: '[JMeter property] Set endpoint IP address'),
    //     string(name: 'REMOTE_HOSTS', description: 'Provide a list of JWorker servers or left field empty to run test on JMaster node'),
    //     fileSystemListParameterDefinition
    // ])
// ])

node {
    stage('JMeter') {
        // set JMeter properties
        def (jmeter_remote_properties_map, jmeter_local_properties_map)              = [[:],[:]]
        def (jmeter_local_properties, jmeter_remote_properties, jmeter_remote_hosts) = ["","",""]

        jmeter_remote_properties_map['USERS']                  = USERS
        jmeter_remote_properties_map['RAMPUP']                 = RAMPUP
        jmeter_remote_properties_map['DURATION']               = DURATION
        jmeter_remote_properties_map['HOST']                   = HOST
        jmeter_remote_properties_map['IP']                     = IP

        jmeter_local_properties_map['server.rmi.ssl.disable']  = "true"
        jmeter_local_properties_map['BUILD_NUMBER']            = env.BUILD_NUMBER

        jmeter_local_properties  = collect_properties(jmeter_local_properties_map, "-J")

        // set remote JWorker hosts
        if(REMOTE_HOSTS.trim()) {
            jmeter_remote_hosts      += "-R ${REMOTE_HOSTS}"
            jmeter_remote_properties = collect_properties(jmeter_remote_properties_map, "-G")
        } else {
            jmeter_local_properties  += " " + collect_properties(jmeter_remote_properties_map, "-J")
        }
        jmeter_remote_hosts = jmeter_remote_hosts.trim()

        String jmeter_image_str = sh(returnStdout: true, script: 'docker images --format "{{ .Repository }}:{{ .Tag }}" | grep jmeter:latest').trim()

        jmeter_image      = docker.image(jmeter_image_str)
        jmeter_container  = jmeter_image.run("--network=host", "sleep infinity")

        try { 
            // copy JMeter scenario into JMeter container
            cp(false, "${jmeterScenariosPath}/${SCENARIO}", jmeter_container.id, "/tmp/${SCENARIO}")

            // command to run JMeter test
            cmd = """
            mkdir ${testResultsPath} && 
            \\\${JMETER_BIN}/jmeter -n -t /tmp/${SCENARIO} -l ${testResultsPath}/log.jtl -e -o ${testResultsPath}/TestResults ${jmeter_local_properties} ${jmeter_remote_properties} ${jmeter_remote_hosts}
            """
            exec(jmeter_container.id, cmd)

            // copy build results into jmeterScenariosPath
            cp(jmeter_container.id, testResultsPath, false, "/var/jenkins_home/")
        } catch (FlowInterruptedException e) {
            handleError(e)
        } catch(Exception e) { 
            handleError(e)
        } finally {
            jmeter_container.stop()
            cleanWs()
        }  
    }
}

def exec(container_id, cmd) {
    sh "docker exec ${container_id} bash -c \"${cmd}\""
}

def cp (container_id1, dest1, container_id2, dest2) {
    if (container_id1 && container_id2) {
        println("You cant copy between containers")
        throw new Exception("docker cp error") 
    } else if (container_id1) {
        sh "docker cp ${container_id1}:${dest1} ${dest2}"
    } else if (container_id2) {
        sh "docker cp ${dest1} ${container_id2}:${dest2}"
    } else {
        sh "docker cp ${dest1} ${dest2}"
    }
}

def collect_properties(Map properties_map, String letter) {
    String str = ""
    for(property in properties_map) {
        if(property.getValue().trim()) {
            str += "${letter}${property} "
        }
    }
    return str.trim()
}

def handleError(exception, status = 'FAILURE') {
    println(exception.message)
    currentBuild.result = status
    throw exception
}
