#!/bin/bash

set -e

/run.sh "$@" &

GRAFANA_API=http://localhost:3000
GRAFANA_USER=admin
GRAFANA_PASS=admin
CREDS=$GRAFANA_USER:$GRAFANA_PASS
DATASOURCES_PATH=/home/grafana/datasources
DASHBOARDS_PATH=/home/grafana/dashboards

add_header() {
    headers=("-H" "$1" "${headers[@]}")
}

add_datasources() {
    for datasource in $(ls -1 ${DATASOURCES_PATH}/*.json); do
        curl -sS -X POST "${headers[@]}" "${GRAFANA_API}" --data @${datasource} "${GRAFANA_API}/api/datasources" -o /dev/null
    done
}

add_dashboards() {
    for dashboard in $(ls -1 ${DASHBOARDS_PATH}/*.json); do
        tmp=`mktemp`
        cat ${dashboard} | jq '.id = null | { dashboard:., inputs:[.__inputs[] | .value = .label | del(.label)], overwrite: true }' > $tmp
        curl -sS -X POST "${headers[@]}" --data @$tmp "${GRAFANA_API}/api/dashboards/db" -o /dev/null
        rm -f $tmp
    done
}

get_token() {
    TOKEN_API_RESPONSE=$(curl -sS -X POST -u $CREDS "${headers[@]}" -d '{"name":"apikeycurl", "role": "Admin", "secondsToLive": 150}' "${GRAFANA_API}/api/auth/keys")
    TOKEN=`echo $TOKEN_API_RESPONSE | jq -r '.key'`
    echo $TOKEN
}

set_headers() {
    headers=(
        "Accept: application/json"
        "Content-Type: application/json"
    )
    IFS=$'\n' i=0
    for elem in ${headers[@]}; do headers[i]="-H"; headers[i+1]="$elem"; ((i+=2)); done
}

token_exists() {
    TOKEN_API_RESPONSE=$(curl -sS -X GET -u $CREDS "${headers[@]}" "${GRAFANA_API}/api/auth/keys?includeExpired=true")
    EXISTS=$(echo ${TOKEN_API_RESPONSE} | jq '.[] | select(.name=="apikeycurl")')
    [ -n "$EXISTS" ] && true || false
}

wait_api() {
    set -- curl -sS -X GET "${headers[@]}" ${GRAFANA_API}/ping -o /dev/null
    set +e
    while : ; do
        "$@"
        [[ $? -eq 0 ]] && break
            echo "API isn't ready. Sleep 5."
            sleep 5;
    done
    set -e
}

set_headers

echo "Wait API ready."
wait_api
echo "API ready."

if token_exists; then
    echo "Token exists."
    echo "Skip setup."
else
    echo "Start setup."
    API_TOKEN=$(get_token)
    add_header "Authorization: Bearer $API_TOKEN"
    add_datasources
    echo "Datasource(s) added."
    add_dashboards
    echo "Dashboard(s) added."
fi

wait $!
